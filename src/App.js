import './App.css';

import 'primeflex/primeflex.css';
import Episodes from './app/components/episodes/episodes'

function App() {
  return (
    <div className="App">
      <Episodes></Episodes>
    </div>
  );
}

export default App;
