const environment = {
    development: {
        api: {
            rickAndMorty: {
                url: "https://rickandmortyapi.com/api/",
                graphql: "https://rickandmortyapi.com/graphql"
            }
        },
    },
    test: {
        api: {
            rickAndMorty: {
                url: "https://rickandmortyapi.com/api/",
                graphql: "https://rickandmortyapi.com/graphql"
            }
        },
    },
    staging: {
        api: {
            rickAndMorty: {
                url: "https://rickandmortyapi.com/api/",
                graphql: "https://rickandmortyapi.com/graphql"
            }
        },
    },
    production: {
        api: {
            rickAndMorty: {
                url: "https://rickandmortyapi.com/api/",
                graphql: "https://rickandmortyapi.com/graphql"
            }
        },
    }
}

export default environment;
