import axios from 'axios'

import { ApolloClient, InMemoryCache } from '@apollo/client';
import environment from '../../../../config/environment'
/**
 * Setup multiple clients
 */
const clients = {
    /**
     * For Rick and Morty REST
     */
    axios : axios.create({
        baseURL: environment[process.env.NODE_ENV].api.rickAndMorty.url,
        timeout: 1000
    }),
    /**
     * For Rick and Morty graphQL
     */
    graphql : new ApolloClient({
        uri: environment[process.env.NODE_ENV].api.rickAndMorty.graphql,
        cache: new InMemoryCache()
    })
}
export default clients
