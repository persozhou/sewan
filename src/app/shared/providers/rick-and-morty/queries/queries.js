import { gql } from '@apollo/client';
import clients from '../rickAndMorty';

const queriesProviders = {

    /**
     * Use Rest Api Rick And Morty
     * @param {string} type 
     */
    getAllByType: async (type) => {
        try {
            const response = await clients.axios.get(type)
            return response
        } catch (err) {
            throw err
        }
    },

    /**
     * Use Rest Api Rick And Morty
     * @param {string} type 
     * @param {number} id
     */
    getAllByTypeAndId: async (type, id) => {
        try {
            const response = await clients.axios.get(`${type}/${id}`)
            return response
        } catch (err) {
            throw err
        }
    },
    /**
     * Use Appollo Client
     * @param {number} page default value = 1
     * @param {string} name default value = undefined
     */
    getInformations: async (page = 1, name = undefined) => {

        try {
            const filter = typeof (name) === "undefined" ? `page: ${page}` : `page : ${page}, filter: {name: "${name}"}`
            const response = await clients.graphql.query({
                query: gql`
                    query {
                        episodes(${filter}) {
                            info {
                              count
                              pages
                            }
                            results {
                                    name
                              air_date
                              episode
                              characters {
                                name
                                image
                                id
                              }
                            }
                        }
                    }
                `
            })
            return response
        } catch (err) {
            throw err
        }
    }
}

export default queriesProviders;