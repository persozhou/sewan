import Episodes from '../../../../components/episodes/episodes';
import queries from './queries'

describe("Test queries services", () => {

    test('getAllByType', () => {
        return queries.getAllByType("episode").then( res => {
            expect(res.data.info).toBeDefined();
            expect(res.data.results).toBeDefined();
            expect(Array.isArray(res.data.results)).toBeTruthy();
            expect((res.data.results[0].name)).toEqual('Pilot');
        })
    });

    test('getAllByTypeAndId', () => {
        return queries.getAllByTypeAndId("episode", 1).then( res => {
            expect(res.data.info).toBeUndefined();
            expect(res.data.results).toBeUndefined();
            expect(typeof(res.data)).toEqual("object");
            expect((res.data.name)).toEqual('Pilot');
        })
    });

    test('getAllByTypeAndId', () => {
        return queries.getInformations().then( res => {
            expect(res.data.episodes.info).toBeDefined();
            expect(res.data.episodes.results).toBeDefined();
            expect(typeof(res.data.episodes)).toEqual("object");
            expect(Array.isArray(res.data.episodes.results)).toBeTruthy();
        })
    });
})