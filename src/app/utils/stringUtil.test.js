import stringUtil from './stringUtil'

describe('Test stringUtil', () => {
    test("Test trancateTexte", () => {
        expect(stringUtil.truncateText("Rick And Morty")).toEqual("Rick...")
        expect(stringUtil.truncateText("RickAndMorty")).toEqual("RickAndMorty...")
        expect(stringUtil.truncateText("")).toEqual("...")
    })

})