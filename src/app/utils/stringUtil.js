const stringUtil = {
    /**
     * Use to truncate text by add ... at the end of the text when the text is too long
     * @param {string} text
     */
    truncateText: (text) => {
        return text.split(' ')[0] + '...'
    }
}

export default stringUtil