import react from 'react';

import stringUtil from '../../../utils/stringUtil';

class ItemEpisodes extends react.Component {

    render() {
        const item =  this.props.episode
        const characters = []

        for (const [index, caractere] of item.characters.entries()) {
            characters.push(
                <div key={index} style={{ maxWidth: 110 }}>
                    <img src={caractere.image} alt={caractere.name} style={{ maxWidth: 80 }}></img>
                    <a href="#" title={caractere.name}>{stringUtil.truncateText(caractere.name)}</a>
                </div>
            )
        }

        return (
            <div>
                <div style={{ display: "flex" }}>
                    <div className="p-col" style={{ maxWidth: 250 }}>
                        <div className="box box-stretched">Episode : {item.episode}</div>
                    </div>
                    <div style={{ maxWidth: "80vw" }}>
                        <div>
                            <div>
                                <div>
                                    <div className="box"> Name : {item.name}</div>
                                </div>
                                <div>
                                    <div className="box">Air diffusion : {item.air_date}</div>
                                </div>
                            </div>
                            <div style={{ display: "flex", flexDirection: "row", overflowY: "auto", height: 120 }}>
                                {characters}
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default ItemEpisodes