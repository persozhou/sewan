import react from 'react';
import queries from '../../../app/shared/providers/rick-and-morty/queries/queries'


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import TextField from '@material-ui/core/TextField';

import Loader from 'react-loader-spinner'

import ItemEpisodes from './itemEpisodes/itemEpisodes'

class Episodes extends react.Component {

    constructor(props) {
        super(props)
        this.state = {
            Informations: {},
            Episodes: [],
            page: {
                totalItem: undefined,
                current: 0,
                last: undefined
            },
            loading: {
                screen: true,
                dataList: false
            }
        };
        this.onScroll = this.onScroll.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
    }

    /**
     * Fetch data when we are at the bottom of the page
     */
    onScroll() {
        if ((window.innerHeight + window.scrollY + 1) >= document.body.offsetHeight &&
            this.state.page.current < this.state.page.last) {
            this.setState({
                page: { current: this.state.page.current + 1 },
                loading: { dataList: true }
            })
            queries.getInformations(this.state.page.current).then(res => {
                this.setState({
                    Episodes: this.state.Episodes.concat(res.data.episodes.results),
                    Informations: res.data.episodes.info,
                    loading: { dataList: false }
                })
            })

        }
    }

    handleSearch(event) {
        if (event.key === "Enter") {
            this.setState({ loading: { screen: true } })
            queries.getInformations(1, event.target.value).then(res => {
                this.setState({
                    Episodes: res.data.episodes.results,
                    Informations: res.data.episodes.info,
                    page: {
                        totalItem: res.data.episodes.info.count,
                        last: res.data.episodes.info.pages,
                        current: 1
                    },
                    loading: { screen: false }
                })
            })
        }
    }

    componentDidMount() {
        if (!this.state.Episodes.length) {
            this.setState({ loading: { screen: true } })
            queries.getInformations().then(res => {
                this.setState({
                    Episodes: res.data.episodes.results,
                    Informations: res.data.episodes.info,
                    page: {
                        totalItem: res.data.episodes.info.count,
                        last: res.data.episodes.info.pages,
                        current: 1
                    },
                    loading: { screen: false }
                })
            })
        }
    }

    componentDidUpdate() {
        window.addEventListener('scroll', this.onScroll)
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll)
    }

    render() {
        let loaderDatalist, body
        if (this.state.loading.dataList) {
            loaderDatalist =
                <TableRow key="loader">
                    <TableCell>
                        <Loader
                            type="Puff"
                            color="#00BFFF"
                            height={100}
                            width={100}
                        />
                    </TableCell>
                </TableRow>
        }
        if (this.state.Episodes.length) {
            body = this.state.Episodes.map((row) => (
                <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                        <ItemEpisodes episode={row}></ItemEpisodes>
                    </TableCell>
                </TableRow>
            ))
        } else {
            body = <TableRow key="empty">
                <TableCell>
                    No Episodes found
                </TableCell>
            </TableRow>
        }
        return (
            <div>
                <TextField
                    id="standard-full-width"
                    label="Search"
                    style={{ margin: 8, position: 'sticky', top: 0, backgroundColor: 'white' }}
                    placeholder="Find episodes"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onKeyDown={this.handleSearch}
                />
                {this.state.loading.screen ? (<Loader
                    type="Puff"
                    color="#00BFFF"
                    height={100}
                    width={100}

                />) : (
                        <div>
                            <TableContainer component={Paper}>
                                <Table aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Episodes Rick and Morty</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {body}
                                        {loaderDatalist}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            {loaderDatalist}
                        </div>
                    )}
            </div>
        )
    }
}

export default Episodes