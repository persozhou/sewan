# Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Test Sewan : Frontend CSS / React JS / GraphQL
Project build with reactJS 
Developed by Patrick Zhou
### Modules used for the project 

[primereact](https://primefaces.org/primereact) : wanted to use DataScoller but it didn't update after the state changed => unused now
[axios](https://www.npmjs.com/package/axios) : use to test the rest api
[apollo](https://www.npmjs.com/package/apollo) : use to make query to the graplQL Rick and Morty
[material UI](https://material-ui.com/) : use to display data
[react-loader-spinner](https://www.npmjs.com/package/react-loader-spinner) : use as loader

### Improvement

Add test for episodes and Itempisodes
Update queries.test.js by adding error case
Add errors management  
Better CSS
Create page for characters and locations
